<?php

namespace Drupal\Tests\bundle_convert\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the ConfigurableFieldManager class.
 *
 * @coversDefaultClass \Drupal\bundle_convert\BundleConverter
 *
 * @group bundle_convert
 */
class BundleConverterTest extends EntityKernelTestBase {

  use NodeCreationTrait {
    createNode as drupalCreateNode;
  }
  use ContentTypeCreationTrait {
    createContentType as drupalCreateContentType;
  }
  use EntityReferenceTestTrait;

  /**
   * Enable modules.
   *
   * @var array
   */
  protected static $modules = [
    'bundle_convert',
    'node',
  ];

  /**
   * The bundle converter service.
   *
   * @var \Drupal\bundle_convert\BundleConverterInterface
   */
  protected $bundleConverter;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A test node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installConfig(['node']);
    $this->installConfig('filter');

    // Create a page node type.
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);
    $this->node = $this->drupalCreateNode(['type' => 'page']);

    $this->bundleConverter = $this->container->get('bundle_convert.converter');
    $this->database = $this->container->get('database');
  }

  /**
   * @covers ::isApplicableEntityType
   * @covers ::isApplicableBundle
   * @covers ::getApplicableEntityTypes
   * @covers ::getApplicableBundles
   */
  public function testApplicable() {
    // There aren't multiple bundles available for
    // any entity types at this point.
    $this->assertEmpty($this->bundleConverter->getApplicableEntityTypes());
    // Users do not have bundles.
    $this->assertFalse($this->bundleConverter->isApplicableEntityType('user'));
    // Entity type with one bundle is not applicable.
    $this->assertFalse($this->bundleConverter->isApplicableEntityType('node'));
    $this->assertEmpty($this->bundleConverter->getApplicableBundles('node'));
    // Article node type does not exist yet.
    $this->assertFalse($this->bundleConverter->isApplicableBundle('node', 'article'));
    // Create an Article node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);
    // Node is now applicable.
    $this->assertTrue($this->bundleConverter->isApplicableEntityType('node'));
    // Article exists and is applicable.
    $this->assertTrue($this->bundleConverter->isApplicableBundle('node', 'article'));
    // Node should be only applicable entity type.
    $this->assertArrayHasKey('node', $this->bundleConverter->getApplicableEntityTypes());
    $this->assertCount(1, $this->bundleConverter->getApplicableEntityTypes());
    // Page and article bundles are applicable for node.
    $this->assertEquals(
      ['page', 'article'],
      array_keys($this->bundleConverter->getApplicableBundles('node'))
    );
  }

  /**
   * @covers ::getBaseDataTables
   * @covers ::getDedicatedFieldTablesArray
   * @covers ::getDedicatedFieldTablesArrayDiverge
   */
  public function testTables() {
    // Base and data tables.
    $this->assertEquals(['node', 'node_field_data'], $this->bundleConverter->getBaseDataTables('node'));
    $this->assertEquals(['users', 'users_field_data'], $this->bundleConverter->getBaseDataTables('user'));

    // Roles is a base field but requires dedicated storage because
    // it is an entity_reference field.
    $this->assertEquals(['roles' => ['user__roles']], $this->bundleConverter->getDedicatedFieldTablesArray('user', 'user'));

    // Body field is not a base field, therefore it has a dedicated table.
    // Nodes are revisionable so there is a revision table for body field.
    // Article node bundle does not exist yet.
    $this->assertEmpty($this->bundleConverter->getDedicatedFieldTablesArray('node', 'article'));

    // Create an Article node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    $body_field_tables = [
      'node__body',
      'node_revision__body',
    ];

    // Check article fields.
    $this->assertEquals(
      ['body' => $body_field_tables],
      $this->bundleConverter->getDedicatedFieldTablesArray('node', 'article')
    );
    // Page and article node bundles should have same fields.
    $this->assertEmpty($this->bundleConverter->getDedicatedFieldTablesArrayDiverge('node', 'page', 'article'));

    // Create a new field for node page bundle.
    $this->createEntityReferenceField(
      'node',
      'page',
      'test_page_field',
      'Test page field',
      'node',
      'default',
      ['target_bundles' => ['page']],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );
    $test_page_field_tables = [
      'node__test_page_field',
      'node_revision__test_page_field',
    ];

    $this->assertEquals([
      'body' => $body_field_tables,
      'test_page_field' => $test_page_field_tables,
    ],
      $this->bundleConverter->getDedicatedFieldTablesArray('node', 'page')
    );

    // Page now has an extra field compared to article.
    $this->assertEquals(
      ['test_page_field' => $test_page_field_tables],
      $this->bundleConverter->getDedicatedFieldTablesArrayDiverge('node', 'page', 'article')
    );
    // test_page_field would be orphaned if converting from page to article.
    $this->assertEquals(
      ['test_page_field' => $test_page_field_tables],
      $this->bundleConverter->getOrphanedDedicatedFieldTables('node', 'page', 'article')
    );
    // There should be no orphaned fields when converting from article to page.
    $this->assertEmpty($this->bundleConverter->getOrphanedDedicatedFieldTables('node', 'article', 'page'));
    // There should be no empty fields when converting from page to article.
    $this->assertEmpty($this->bundleConverter->getEmptyDedicatedFieldTables('node', 'page', 'article'));
    // test_page_field would be empty if converting from article to page.
    $this->assertEquals(
      ['test_page_field' => $test_page_field_tables],
      $this->bundleConverter->getEmptyDedicatedFieldTables('node', 'article', 'page')
    );

    // Create a new field for node article bundle.
    $this->createEntityReferenceField(
      'node',
      'article',
      'test_article_field',
      'Test article field',
      'node',
      'default',
      ['target_bundles' => ['article']],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );
    $test_article_field_tables = [
      'node__test_article_field',
      'node_revision__test_article_field',
    ];

    $this->assertEquals([
      'body' => $body_field_tables,
      'test_article_field' => $test_article_field_tables,
    ],
      $this->bundleConverter->getDedicatedFieldTablesArray('node', 'article')
    );

    $this->assertEquals([
      'test_page_field' => $test_page_field_tables,
      'test_article_field' => $test_article_field_tables,
    ],
      $this->bundleConverter->getDedicatedFieldTablesArrayDiverge('node', 'page', 'article')
    );

    // Node page bundle has one field that article bundle does not have.
    $this->assertEquals([
      'test_page_field' => $test_page_field_tables,
    ],
      $this->bundleConverter->getOrphanedDedicatedFieldTables('node', 'page', 'article')
    );
    $this->assertEquals([
      'test_page_field' => $test_page_field_tables,
    ],
      $this->bundleConverter->getEmptyDedicatedFieldTables('node', 'article', 'page')
    );

    // Node article bundle has one field that page bundle does not have.
    $this->assertEquals([
      'test_article_field' => $test_article_field_tables,
    ],
      $this->bundleConverter->getOrphanedDedicatedFieldTables('node', 'article', 'page')
    );
    $this->assertEquals([
      'test_article_field' => $test_article_field_tables,
    ],
      $this->bundleConverter->getEmptyDedicatedFieldTables('node', 'page', 'article')
    );
  }

  /**
   * @covers ::convertEntity
   */
  public function testConvertEntity() {
    $this->assertEquals('page', $this->node->bundle());
    // Article node bundle does not exist.
    $this->assertFalse($this->bundleConverter->convertEntity($this->node, 'article'));
    // Ensure the database was not updated for non_applicable bundle.
    $this->assertEquals('page', $this->node->bundle());

    // Create an Article node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    // Create a new field for node article bundle.
    $this->createEntityReferenceField(
      'node',
      'article',
      'test_article_field',
      'Test article field',
      'node',
      'default',
      ['target_bundles' => ['page']],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );

    // Create another page node so that we can be sure all deltas for the
    // entity reference field are converted properly.
    $another_page_node = $this->drupalCreateNode(['type' => 'page']);
    $article = $this->drupalCreateNode([
      'type' => 'article',
      'test_article_field' => [$this->node, $another_page_node],
    ]);
    $this->assertEquals('article', $article->bundle());
    $this->assertEntityFieldDatabaseRowsCount($article, 'node__test_article_field', 2, 'article');
    $this->assertEntityFieldDatabaseRowsCount($article, 'node_revision__test_article_field', 2, 'article');

    // Check the bundle values are correct in the database.
    $this->assertEntityDatabaseBundle($article, 'article');

    // Convert the article to a page.
    $this->bundleConverter->convertEntity($article, 'page');
    $article = $this->reloadEntity($article);

    // Ensure the bundle is now page.
    $this->assertEquals('page', $article->bundle());
    $this->assertEntityDatabaseBundle($article, 'page');

    // Ensure the test_article_field table is cleared since
    // the page bundle does not have that field. Purposefully not
    // providing the bundle to make sure there is no orphaned data in the table.
    $this->assertEntityFieldDatabaseRowsCount($article, 'node__test_article_field', 0);
    $this->assertEntityFieldDatabaseRowsCount($article, 'node_revision__test_article_field', 0);
  }

  /**
   * @covers ::bulkConvert
   */
  public function testBulkConvert() {
    // Create an Article node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);
    // Create a new field for node article bundle.
    $this->createEntityReferenceField(
      'node',
      'article',
      'test_article_field',
      'Test article field',
      'node',
      'default',
      ['target_bundles' => ['page']],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );

    $nodes = [];
    $create_nodes_count = 5;
    $count = 0;
    while ($create_nodes_count > $count) {
      $new_node = $this->drupalCreateNode([
        'type' => 'article',
        'test_article_field' => [$this->node],
      ]);
      $this->assertEquals('article', $new_node->bundle());
      $this->assertEntityDatabaseBundle($new_node, 'article');

      $nodes[$new_node->id()] = $new_node;
      $count++;
    }
    $this->assertCount($create_nodes_count, $nodes);

    // The test_article_field should only have the same
    // number of rows as there are number of articles.
    $this->assertFieldDatabaseRowsCount('node__test_article_field', $create_nodes_count);
    $this->assertFieldDatabaseRowsCount('node_revision__test_article_field', $create_nodes_count);
    // The body field should have the same number of rows
    // as total amounts of nodes. Purposefully not checking
    // the bundle of the rows.
    $this->assertFieldDatabaseRowsCount('node__body', $create_nodes_count + 1);
    $this->assertFieldDatabaseRowsCount('node_revision__body', $create_nodes_count + 1);

    // Passing entity ids with delete_from_bundle is not allowed.
    $this->expectException(\InvalidArgumentException::class);
    $this->bundleConverter->bulkConvert('node', 'article', 'page', array_keys($nodes), ['delete_from_bundle' => TRUE]);
    // Convert all page nodes to articles.
    $this->bundleConverter->bulkConvert('node', 'article', 'page', [], ['delete_from_bundle' => TRUE]);

    // Check the conversion was successful.
    foreach ($nodes as $nid => $node) {
      $node = $this->reloadEntity($node);
      $nodes[$nid] = $node;
      $this->assertEquals('page', $node->bundle());
      $this->assertEntityDatabaseBundle($node, 'page');
    }

    // Ensure the test_article_field table is cleared since
    // the page bundle does not have that field. Purposefully not
    // providing the bundle to make sure there is no orphaned
    // data in the table.
    $this->assertFieldDatabaseRowsCount('node__test_article_field', 0);
    $this->assertFieldDatabaseRowsCount('node_revision__test_article_field', 0);
    // The body field should all be page bundle.
    $this->assertFieldDatabaseRowsCount('node__body', $create_nodes_count + 1, 'page');
    $this->assertFieldDatabaseRowsCount('node_revision__body', $create_nodes_count + 1, 'page');
  }

  /**
   * Asserts the entity is of the provided bundle via database query.
   *
   * Manually checks each base table and field table for the bundle.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $expected_bundle
   *   The expected bundle.
   */
  protected function assertEntityDatabaseBundle(EntityInterface $entity, $expected_bundle) {
    $this->assertEntityBaseTableBundle($entity, $expected_bundle);
    $this->assertDedicatedFieldTableBundle($entity, $expected_bundle);
  }

  /**
   * Asserts the entity bundle value in the base tables for the entity type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $expected_bundle
   *   The expected bundle.
   */
  protected function assertEntityBaseTableBundle(EntityInterface $entity, $expected_bundle) {
    $entity_type = $entity->getEntityType();
    $bundle_key = $entity_type->getKey('bundle');
    $id_key = $entity_type->getKey('id');

    $base_tables = $this->bundleConverter->getBaseDataTables($entity->getEntityTypeId());
    foreach ($base_tables as $base_table) {
      $count = $this->database->select($base_table, 't')
        ->fields('t')
        ->condition($bundle_key, $expected_bundle)
        ->condition($id_key, $entity->id())
        ->countQuery()
        ->execute()
        ->fetchField();
      $this->assertEquals(1, (int) $count);
    }
  }

  /**
   * Asserts the entity field bundle in the entities dedicated field tables.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $expected_bundle
   *   The expected bundle.
   */
  protected function assertDedicatedFieldTableBundle(EntityInterface $entity, $expected_bundle) {
    $fields = $this->bundleConverter->getDedicatedFieldTablesArray($entity->getEntityTypeId(), $expected_bundle);
    foreach ($fields as $field_name => $tables) {
      $expected_count = $entity->get($field_name)->count();
      foreach ($tables as $table) {
        $this->assertEntityFieldDatabaseRowsCount($entity, $table, $expected_count, $expected_bundle);
      }
    }
  }

  /**
   * Asserts the number of rows in the provided field table for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $field_table
   *   The field table name.
   * @param int $expected_count
   *   The expected count.
   * @param string $expected_bundle
   *   (optional) The expected bundle.
   */
  protected function assertEntityFieldDatabaseRowsCount(EntityInterface $entity, $field_table, $expected_count, $expected_bundle = NULL) {
    $query = $this->database->select($field_table, 't')
      ->fields('t')
      ->condition('entity_id', $entity->id());

    if ($expected_bundle) {
      $query->condition('bundle', $expected_bundle);
    }

    $count = $query->countQuery()
      ->execute()
      ->fetchField();

    $this->assertEquals($expected_count, (int) $count);
  }

  /**
   * Asserts the number of rows in the provided field table.
   *
   * @param string $field_table
   *   The field table name.
   * @param int $expected_count
   *   The expected count.
   * @param string $expected_bundle
   *   (optional) The expected bundle.
   */
  protected function assertFieldDatabaseRowsCount($field_table, $expected_count, $expected_bundle = NULL) {
    $query = $this->database->select($field_table, 't')
      ->fields('t');

    if ($expected_bundle) {
      $query->condition('bundle', $expected_bundle);
    }

    $count = $query->countQuery()
      ->execute()
      ->fetchField();

    $this->assertEquals($expected_count, (int) $count);
  }

}

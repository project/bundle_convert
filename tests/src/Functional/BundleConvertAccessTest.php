<?php

namespace Drupal\Tests\bundle_convert\Functional;

use Drupal\Core\Url;
use Drupal\system\Entity\Action;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests bundle convert access.
 *
 * @group bundle_convert
 */
class BundleConvertAccessTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'block',
    'bundle_convert',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A test node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a page node type.
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);

    // Create an article node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    $this->node = $this->drupalCreateNode();

    $this->drupalPlaceBlock('local_tasks_block');

    $this->config('bundle_convert.settings')->set('entity_types', [
      'node' => [
        'action' => TRUE,
        'bulk' => TRUE,
        'local_task' => TRUE,
      ],
    ])->save();
    // Flush cache for local task to be generated.
    drupal_flush_all_caches();

    // Create the node bundle_convert action.
    $action = Action::create([
      'id' => 'node_convert_bundle_action',
      'label' => t('Convert @entity_type bundle', ['@entity_type' => 'Node']),
      'type' => 'node',
      'configuration' => [],
      'plugin' => 'entity:convert_bundle_action:node',
    ]);
    $action->save();
  }

  /**
   * Tests the bundle convert action access.
   */
  public function testBundleConvertActionAccess() {
    $session = $this->assertSession();

    $permissions = [
      'access content overview',
      'bypass node access',
    ];

    $account = $this->drupalCreateUser($permissions);
    $this->drupalLogin($account);

    // Load the role that was created.
    $roles = $account->getRoles();
    $role = Role::load($roles[0]);

    // The action should not exist, no permission granted
    // for bundle_convert action.
    $this->drupalGet('/admin/content');
    $session->optionNotExists('action', 'node_convert_bundle_action');

    // The route where the action is executed should deny access.
    $this->drupalGet(Url::fromRoute('entity.node.bundle_convert_multiple_form'));
    $session->statusCodeEquals(403);

    // Grand bundle_convert action permission.
    $this->grantPermissions($role, ['access bundle_convert action']);

    // The bundle_convert action should now be available.
    $this->drupalGet('/admin/content');
    $session->optionExists('action', 'node_convert_bundle_action');

    // The route where the action is executed should be allowed.
    $this->drupalGet(Url::fromRoute('entity.node.bundle_convert_multiple_form'));
    $session->statusCodeEquals(200);
  }

  /**
   * Tests the bundle convert local task access.
   */
  public function testBundleConvertLocalTaskAccess() {
    $session = $this->assertSession();

    $permissions = [
      'bypass node access',
    ];

    $account = $this->drupalCreateUser($permissions);
    $this->drupalLogin($account);

    // Load the role that was created.
    $roles = $account->getRoles();
    $role = Role::load($roles[0]);

    $this->drupalGet($this->node->toUrl('edit-form'));
    $session->linkNotExists('Convert bundle');

    $this->drupalGet($this->node->toUrl()->toString() . '/bundle-convert');
    $session->statusCodeEquals(403);

    $this->grantPermissions($role, ['access bundle_convert local_task']);

    $this->drupalGet($this->node->toUrl()->toString() . '/bundle-convert');
    $session->statusCodeEquals(200);

    $this->drupalGet($this->node->toUrl('edit-form'));
    $session->linkExists('Convert bundle');
  }

  /**
   * Tests the bundle convert bulk access.
   */
  public function testBundleConvertBulkAccess() {
    $session = $this->assertSession();

    $account = $this->drupalCreateUser();
    $this->drupalLogin($account);

    // Load the role that was created.
    $roles = $account->getRoles();
    $role = Role::load($roles[0]);

    $this->drupalGet(Url::fromRoute('bundle_convert.bulk_form'));
    $session->statusCodeEquals(403);

    $this->grantPermissions($role, ['access bundle_convert bulk']);

    $this->drupalGet(Url::fromRoute('bundle_convert.bulk_form'));
    $session->statusCodeEquals(200);
  }

}

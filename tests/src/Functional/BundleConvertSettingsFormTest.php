<?php

namespace Drupal\Tests\bundle_convert\Functional;

use Drupal\system\Entity\Action;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the bundle convert settings form ui.
 *
 * @group bundle_convert
 */
class BundleConvertSettingsFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'block',
    'bundle_convert',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Tests the bundle convert settings page.
   */
  public function testSettingsPage() {
    $session = $this->assertSession();

    // Check access denied without 'administer bundle_convert' permission.
    $this->drupalGet('/admin/config/content/bundle-convert/settings');
    $session->statusCodeEquals(403);

    // Check access granted by 'administer bundle_convert' permission.
    $permissions = [
      'administer site configuration',
      'bypass node access',
      'administer bundle_convert',
      'access bundle_convert action',
      'access bundle_convert bulk',
      'access bundle_convert local_task',
    ];

    $account = $this->drupalCreateUser($permissions);
    $this->drupalLogin($account);
    $this->drupalGet('/admin/config/content/bundle-convert/settings');
    $session->statusCodeEquals(200);

    // Check node settings do not exists because there are no bundles available.
    $session->fieldNotExists('settings[node][action]');
    $session->fieldNotExists('settings[node][bulk]');
    $session->fieldNotExists('settings[node][local_task]');

    // Create a page content type.
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $this->drupalGet('/admin/config/content/bundle-convert/settings');

    // Check node settings do not exist because there is only one bundle.
    $session->fieldNotExists('settings[node][action]');
    $session->fieldNotExists('settings[node][bulk]');
    $session->fieldNotExists('settings[node][local_task]');

    // Create an article content type.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
    $this->drupalGet('/admin/config/content/bundle-convert/settings');

    // Check node settings exist now that two bundles are available.
    $this->drupalGet('/admin/config/content/bundle-convert/settings');
    $session->fieldExists('settings[node][action]');
    $session->fieldExists('settings[node][bulk]');
    $session->fieldExists('settings[node][local_task]');

    // Details element should not be open if no settings are enabled.
    $this->assertFalse($this->xpath('//details[@id="edit-settings-node"]')[0]->hasAttribute('open'));

    // Check config is empty.
    $config = \Drupal::config('bundle_convert.settings')->get('entity_types');
    $this->assertEmpty($config);

    // Check the action does not exist.
    $action = Action::load('node_convert_bundle_action');
    $this->assertNull($action);

    // Create a node and check that the local task doesn't exist.
    $node = $this->drupalCreateNode(['uid' => $account->id()]);
    $this->drupalGet($node->toUrl('edit-form'));
    $session->linkNotExists('Convert bundle');

    // Save the settings form.
    $this->drupalGet('/admin/config/content/bundle-convert/settings');
    $values = [
      'settings[node][action]' => 1,
      'settings[node][bulk]' => 1,
      'settings[node][local_task]' => 1,
    ];
    $this->submitForm($values, 'Save');

    // Details element should be open if settings are enabled.
    $this->assertTrue($this->xpath('//details[@id="edit-settings-node"]')[0]->hasAttribute('open'));

    // Check config was updated.
    $config = \Drupal::config('bundle_convert.settings')->get('entity_types');
    $this->assertEquals([
      'node' => [
        'action' => TRUE,
        'bulk' => TRUE,
        'local_task' => TRUE,
      ],
    ], $config);

    // Check the action was created.
    $action = Action::load('node_convert_bundle_action');
    $this->assertNotNull($action);

    // Clear cache so the local task can be created.
    drupal_flush_all_caches();

    // Check the local task exists.
    $this->drupalGet($node->toUrl('edit-form'));
    $session->linkExists('Convert bundle');
  }

}

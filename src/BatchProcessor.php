<?php

namespace Drupal\bundle_convert;

/**
 * The bundle convert batch processor.
 */
class BatchProcessor {

  /**
   * Batch function for converting multiple entities to another bundle.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $to_bundle
   *   The to bundle.
   * @param array $selection
   *   The entities to convert.
   * @param array $context
   *   The context.
   */
  public static function convertMultiple($entity_type_id, $to_bundle, $selection, array &$context = []) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['total_quantity'] = count($selection);
      $context['sandbox']['current'] = 0;
      $context['results']['converted_bundles'] = [];
      $context['results']['error_bundles'] = [];
      $context['results']['total_quantity'] = count($selection);
    }
    $total_quantity = $context['sandbox']['total_quantity'];
    $current = &$context['sandbox']['current'];
    $from_bundle = array_keys($selection)[$current];

    /** @var \Drupal\bundle_convert\BundleConverterInterface $bundle_converter */
    $bundle_converter = \Drupal::service('bundle_convert.converter');
    $success = $bundle_converter->bulkConvert($entity_type_id, $from_bundle, $to_bundle, $selection[$from_bundle]);
    if ($success) {
      $context['results']['converted_bundles'][] = $from_bundle;
    }
    else {
      $context['results']['error_bundles'][] = $from_bundle;
    }
    $current++;

    $context['message'] = t('Converting bundle <em>@from_bundle</em> to bundle <em>@to_bundle</em>',
      ['@from_bundle' => $from_bundle, '@to_bundle' => $to_bundle]
    );
    $context['finished'] = $current / $total_quantity;
  }

  /**
   * Batch function for converting all provided bundles to another bundle.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $from_bundles
   *   The from bundles.
   * @param string $to_bundle
   *   The to bundle.
   * @param array $options
   *   The bundle convert options.
   * @param array $context
   *   The context.
   */
  public static function convertAll($entity_type_id, array $from_bundles, $to_bundle, array $options = [], array &$context = []) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['total_quantity'] = count($from_bundles);
      $context['sandbox']['current'] = 0;
      $context['results']['converted_bundles'] = [];
      $context['results']['error_bundles'] = [];
      $context['results']['total_quantity'] = count($from_bundles);
    }
    $total_quantity = $context['sandbox']['total_quantity'];
    $current = &$context['sandbox']['current'];
    $from_bundle = array_values($from_bundles)[$current];

    /** @var \Drupal\bundle_convert\BundleConverterInterface $bundle_converter */
    $bundle_converter = \Drupal::service('bundle_convert.converter');
    $success = $bundle_converter->bulkConvert($entity_type_id, $from_bundle, $to_bundle, [], $options);
    if ($success) {
      $context['results']['converted_bundles'][] = $from_bundle;
    }
    else {
      $context['results']['error_bundles'][] = $from_bundle;
    }
    $current++;

    $context['message'] = t('Converting bundle <em>@from_bundle</em> to bundle <em>@to_bundle</em>',
      ['@from_bundle' => $from_bundle, '@to_bundle' => $to_bundle]
    );
    $context['finished'] = $current / $total_quantity;
  }

  /**
   * Batch finished callback: display batch statistics.
   *
   * @param bool $success
   *   Indicates whether the batch has completed successfully.
   * @param mixed[] $results
   *   The array of results gathered by the batch processing.
   * @param string[] $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   */
  public static function convertAllFinish($success, array $results, array $operations) {
    if ($success) {
      $converted = count($results['converted_bundles']);
      // If any bundles had errors while converting.
      if (!empty($results['error_bundles'])) {
        \Drupal::messenger()->addWarning(t('Converted %converted out of %total bundles. The following bundles had errors, check the log messages for details. %error_bundles.', [
          '%converted' => $converted,
          '%total' => $results['total_quantity'],
          '%error_bundles' => implode(', ', $results['error_bundles']),
        ]));
      }
      else {
        \Drupal::messenger()->addMessage(\Drupal::translation()->formatPlural(
          $converted,
          'Converted 1 bundle.',
          'Converted @count bundles.'
        ));
      }
    }
    else {
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(t('An error occurred while processing @operation with arguments: @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}

<?php

namespace Drupal\bundle_convert\Controller;

use Drupal\bundle_convert\Form\BundleConvertSingleForm;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Returns responses for Bundle Convert routes.
 */
class BundleConvertController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(RouteMatchInterface $route_match) {
    $entity_type_id = $route_match->getRouteObject()->getDefault('entity_type_id');
    $entity = $route_match->getParameter($entity_type_id);
    return $this->formBuilder()->getForm(BundleConvertSingleForm::class, $entity);
  }

}

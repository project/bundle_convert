<?php

namespace Drupal\bundle_convert;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

/**
 * The bundle converter service.
 */
class BundleConverter implements BundleConverterInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * BundleConverter constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, Connection $connection, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->database = $connection;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public function getBundleConvertSettings() {
    return $this->configFactory->get('bundle_convert.settings')->get('entity_types');
  }

  /**
   * {@inheritDoc}
   */
  public function getBundleConvertEntityTypeSettings($entity_type_id) {
    $settings = $this->getBundleConvertSettings();

    if (!array_key_exists($entity_type_id, $settings)) {
      return [];
    }

    return $settings[$entity_type_id];
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicableEntityType($entity_type_id) {
    $storage = $this->getStorage($entity_type_id);
    if (!$storage instanceof SqlEntityStorageInterface) {
      return FALSE;
    }

    if (!$storage->getEntityType()->hasKey('bundle')) {
      return FALSE;
    }

    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
    return count($bundle_info) > 1;
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicableBundle($entity_type_id, $bundle) {
    if (!$this->isApplicableEntityType($entity_type_id)) {
      return FALSE;
    }

    $bundles = $this->getApplicableBundles($entity_type_id);
    return array_key_exists($bundle, $bundles);
  }

  /**
   * {@inheritDoc}
   */
  public function getApplicableEntityTypes() {
    return array_filter($this->entityTypeManager->getDefinitions(), function ($entity_type) {
      return $this->isApplicableEntityType($entity_type->id());
    });
  }

  /**
   * {@inheritDoc}
   */
  public function getApplicableBundles($entity_type_id) {
    if (!$this->isApplicableEntityType($entity_type_id)) {
      return [];
    }

    return $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
  }

  /**
   * {@inheritDoc}
   */
  public function getBaseDataTables($entity_type_id) {
    $tables = [];

    $storage = $this->getStorage($entity_type_id);
    if ($base_table = $storage->getBaseTable()) {
      $tables[] = $base_table;
    }
    if ($data_table = $storage->getDataTable()) {
      $tables[] = $data_table;
    }

    return $tables;
  }

  /**
   * {@inheritDoc}
   */
  public function getDedicatedFieldTablesArray($entity_type_id, $bundle) {
    $storage = $this->getStorage($entity_type_id);
    $mapping = $storage->getTableMapping();

    // Only include field definitions that have their own dedicated table.
    $field_definitions = array_filter($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle), function ($field_definition) use ($mapping) {
      $storage_definition = $field_definition->getFieldStorageDefinition();
      return $mapping->requiresDedicatedTableStorage($storage_definition);
    });

    return array_map(function ($field_definition) use ($mapping) {
      return $mapping->getAllFieldTableNames($field_definition->getName());
    }, $field_definitions);
  }

  /**
   * {@inheritDoc}
   */
  public function getDedicatedFieldTablesArrayDiverge($entity_type_id, $from_bundle, $to_bundle) {
    $orphaned = $this->getOrphanedDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle);
    $empty = $this->getEmptyDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle);

    return array_merge($orphaned, $empty);
  }

  /**
   * {@inheritDoc}
   */
  public function getOrphanedDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle) {
    $from_fields = $this->getDedicatedFieldTablesArray($entity_type_id, $from_bundle);
    $to_fields = $this->getDedicatedFieldTablesArray($entity_type_id, $to_bundle);

    return array_diff_key($from_fields, $to_fields);
  }

  /**
   * {@inheritDoc}
   */
  public function getEmptyDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle) {
    $from_fields = $this->getDedicatedFieldTablesArray($entity_type_id, $from_bundle);
    $to_fields = $this->getDedicatedFieldTablesArray($entity_type_id, $to_bundle);

    return array_diff_key($to_fields, $from_fields);
  }

  /**
   * {@inheritDoc}
   */
  public function convertEntity(EntityInterface $entity, $to_bundle, $options = []) {
    $entity_type_id = $entity->getEntityTypeId();

    if (!$this->isApplicableBundle($entity_type_id, $to_bundle)) {
      return FALSE;
    }

    return $this->bulkConvert($entity_type_id, $entity->bundle(), $to_bundle, [$entity->id()], $options);
  }

  /**
   * {@inheritDoc}
   */
  public function bulkConvert($entity_type_id, $from_bundle, $to_bundle, $entity_ids = [], $options = ['delete_from_bundle' => FALSE]) {
    // Do nothing if trying to convert to the same bundle.
    if ($from_bundle === $to_bundle) {
      return FALSE;
    }

    // Can only convert bundles that are applicable for the entity type.
    if (!$this->isApplicableBundle($entity_type_id, $from_bundle) || !$this->isApplicableBundle($entity_type_id, $to_bundle)) {
      return FALSE;
    }

    // Only allow deleting from_bundle if ALL entities of
    // the bundle are converted.
    if (!empty($options['delete_from_bundle']) && !empty($entity_ids)) {
      throw new \InvalidArgumentException('The option delete_from_bundle is not valid when passing entity_ids.');
    }

    $storage = $this->getStorage($entity_type_id);
    $entity_type = $storage->getEntityType();
    $bundle_key = $entity_type->getKey('bundle');
    $id_key = $entity_type->getKey('id');

    $transaction = $this->database->startTransaction();
    try {
      // Convert base tables.
      $base_tables = $this->getBaseDataTables($entity_type_id);
      foreach ($base_tables as $base_table) {
        $query = $this->database->update($base_table)
          ->fields([$bundle_key => $to_bundle])
          ->condition($bundle_key, $from_bundle);
        if (!empty($entity_ids)) {
          $query->condition($id_key, $entity_ids, 'IN');
        }
        $query->execute();
      }

      // Delete data from fields that do not exist on to_bundle.
      $field_tables_array_diff = $this->getOrphanedDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle);
      foreach ($field_tables_array_diff as $field_table_array_diff) {
        foreach ($field_table_array_diff as $table) {
          $query = $this->database->delete($table)
            ->condition('bundle', $from_bundle);
          if (!empty($entity_ids)) {
            $query->condition('entity_id', $entity_ids, 'IN');
          }
          $query->execute();
        }
      }

      // Convert fields that exist on from_bundle and to_bundle.
      $field_tables_array = $this->getDedicatedFieldTablesArray($entity_type_id, $from_bundle);
      foreach ($field_tables_array as $field_table_array) {
        foreach ($field_table_array as $table) {
          $query = $this->database->update($table)
            ->fields(['bundle' => $to_bundle])
            ->condition('bundle', $from_bundle);
          if (!empty($entity_ids)) {
            $query->condition('entity_id', $entity_ids, 'IN');
          }
          $query->execute();
        }
      }

      // Rebuild the entity type cache.
      $storage->resetCache();

      // Delete from_bundle if user opted to do so.
      if (!empty($options['delete_from_bundle'])) {
        $this->getStorage($entity_type->getBundleEntityType())
          ->load($from_bundle)
          ->delete();
      }
    }
    catch (\Exception $e) {
      $transaction->rollBack();
      watchdog_exception('bundle_convert', $e);
      return FALSE;
    }

    // Commit the transaction.
    unset($transaction);
    return TRUE;
  }

  /**
   * Gets the entity type storage.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   *   The entity type storage.
   */
  protected function getStorage($entity_type_id) {
    return $this->entityTypeManager->getStorage($entity_type_id);
  }

}

<?php

namespace Drupal\bundle_convert\Access;

use Drupal\bundle_convert\BundleConverterInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for the bundle convert local task route.
 */
class BundleConvertLocalTaskAccessCheck implements AccessInterface {

  /**
   * The bundle converter.
   *
   * @var \Drupal\bundle_convert\BundleConverterInterface
   */
  protected $bundleConverter;

  /**
   * Constructs a new BundleConvertLocalTaskAccessCheck object.
   *
   * @param \Drupal\bundle_convert\BundleConverterInterface $bundle_converter
   *   The bundle converter.
   */
  public function __construct(BundleConverterInterface $bundle_converter) {
    $this->bundleConverter = $bundle_converter;
  }

  /**
   * Checks access to the bundle convert action route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $entity_type_id = $route_match->getParameter('entity_type_id');

    if (!$entity_type_id) {
      return AccessResult::forbidden();
    }

    $bundle_convert_settings = $this->bundleConverter->getBundleConvertEntityTypeSettings($entity_type_id);
    if (empty($bundle_convert_settings['local_task'])) {
      return AccessResult::forbidden();
    }

    $permissions = [
      'access bundle_convert local_task',
    ];

    return AccessResult::allowedIfHasPermissions($account, $permissions);
  }

}

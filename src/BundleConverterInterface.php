<?php

namespace Drupal\bundle_convert;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the interface for the BundleConverter.
 */
interface BundleConverterInterface {

  /**
   * Gets the bundle convert settings.
   *
   * @return array
   *   The bundle convert settings.
   */
  public function getBundleConvertSettings();

  /**
   * Gets the bundle convert settings for the entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return array
   *   The bundle convert settings for the entity type.
   */
  public function getBundleConvertEntityTypeSettings($entity_type_id);

  /**
   * Returns whether the provided entity type is applicable.
   *
   * An entity type is applicable if its storage class implements
   * SqlEntityStorageInterface and has bundles.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return bool
   *   TRUE if the entity type is applicable, FALSE otherwise.
   */
  public function isApplicableEntityType($entity_type_id);

  /**
   * Returns whether the provided bundle is applicable.
   *
   * Ensures the entity type is applicable and that the
   * bundle belongs to the entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The bundle.
   *
   * @return bool
   *   TRUE if the bundle is applicable, FALSE otherwise.
   */
  public function isApplicableBundle($entity_type_id, $bundle);

  /**
   * Gets all entity types that are applicable.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   The applicable entity types.
   */
  public function getApplicableEntityTypes();

  /**
   * Gets the bundles that are applicable.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return array
   *   An array of bundle information where the outer array is keyed by the
   *   bundle name. The inner arrays are associative arrays of bundle
   *   information, such as the label for the bundle.
   */
  public function getApplicableBundles($entity_type_id);

  /**
   * Gets the base data table names for the entity type.
   *
   * Includes both the base and data tables.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return array
   *   The table names.
   */
  public function getBaseDataTables($entity_type_id);

  /**
   * Gets the table names for fields with dedicated tables.
   *
   * A dedicated field table is one that is not stored with the
   * base table for the entity. Fields such as entity references
   * will have dedicated tables even if they are base fields.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   An array of fields that have dedicated storage tables.
   *   The array is keyed by the field machine name, with a value
   *   of an array containing all field table names.  Includes the field
   *   data table and revision table if applicable.
   */
  public function getDedicatedFieldTablesArray($entity_type_id, $bundle);

  /**
   * Returns divergent dedicated field table arrays.
   *
   * Returns all dedicated field tables that are different
   * between the from_bundle and to_bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $from_bundle
   *   The from bundle.
   * @param string $to_bundle
   *   The to bundle.
   *
   * @return array
   *   An array of fields that have dedicated storage tables.
   *   The array is keyed by the field machine name, with a value
   *   of an array containing all field table names.  Includes the field
   *   data table and revision table if applicable.
   */
  public function getDedicatedFieldTablesArrayDiverge($entity_type_id, $from_bundle, $to_bundle);

  /**
   * Returns from_bundle fields that will be orphaned after converting.
   *
   * Returns the field tables that are present on the from_bundle and
   * missing from the to_bundle.  This is useful to determine what
   * data may be orphaned when converting between bundles.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $from_bundle
   *   The from bundle.
   * @param string $to_bundle
   *   The to bundle.
   *
   * @return array
   *   An array of fields that have dedicated storage tables.
   *   The array is keyed by the field machine name, with a value
   *   of an array containing all field table names.  Includes the field
   *   data table and revision table if applicable.
   */
  public function getOrphanedDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle);

  /**
   * Returns from_bundle fields that will be empty after converting.
   *
   * Returns the field tables that are present on the to_bundle and
   * missing from the from_bundle.  This is useful to determine what
   * data may be empty when converting between bundles.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $from_bundle
   *   The from bundle.
   * @param string $to_bundle
   *   The to bundle.
   *
   * @return array
   *   An array of fields that have dedicated storage tables.
   *   The array is keyed by the field machine name, with a value
   *   of an array containing all field table names.  Includes the field
   *   data table and revision table if applicable.
   */
  public function getEmptyDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle);

  /**
   * Converts an entity to another bundle.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $to_bundle
   *   The bundle to convert to.
   * @param array $options
   *   The options.
   *
   * @return bool
   *   TRUE if the conversion was successful, FALSE otherwise.
   */
  public function convertEntity(EntityInterface $entity, $to_bundle, $options = []);

  /**
   * Converts entities from one bundle to another bundle.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $from_bundle
   *   The bundle to convert from.
   * @param string $to_bundle
   *   The bundle to convert to.
   * @param string[] $entity_ids
   *   An optional array of entity ids to convert.
   * @param array $options
   *   The options.
   *
   * @option delete_from_bundle Delete the from_bundle after converting the bundle.
   *   May only be used if all entities of that bundle are being converted.
   *
   * @return bool
   *   TRUE if the conversion was successful, FALSE otherwise.
   *
   * @throws \InvalidArgumentException
   *   When the delete_from_bundle option is TRUE and entity_ids
   *   were provided. Bundles may only be deleted if all entities
   *   of that bundle were converted.
   */
  public function bulkConvert($entity_type_id, $from_bundle, $to_bundle, $entity_ids = [], $options = ['delete_from_bundle' => FALSE]);

}

<?php

namespace Drupal\bundle_convert\Routing;

use Drupal\bundle_convert\BundleConverterInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for bundle convert routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The bundle converter service.
   *
   * @var \Drupal\bundle_convert\BundleConverterInterface
   */
  protected $bundleConverter;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\bundle_convert\BundleConverterInterface $bundle_converter
   *   The bundle converter service.
   */
  public function __construct(BundleConverterInterface $bundle_converter) {
    $this->bundleConverter = $bundle_converter;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->bundleConverter->getApplicableEntityTypes() as $entity_type_id => $entity_type) {
      if ($route = $this->bundleConvertMultipleRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.bundle_convert_multiple_form", $route);
      }
      if ($route = $this->bundleConvertEntityRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.bundle_convert", $route);
      }
    }
  }

  /**
   * Returns the bundle convert multiple route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route
   *   The bundle convert multiple route.
   */
  protected function bundleConvertMultipleRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();
    $route = new Route("/admin/config/content/bundle-convert/$entity_type_id/multiple");
    $route
      ->addDefaults([
        '_form' => '\Drupal\bundle_convert\Form\BundleConvertMultipleForm',
        '_title' => 'Convert ' . $entity_type->getSingularLabel() . ' bundles',
        'entity_type_id' => $entity_type_id,
      ])
      ->setRequirement('_bundle_convert_action_access', 'TRUE');

    return $route;
  }

  /**
   * Returns the bundle convert entity route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The bundle convert entity route.
   */
  protected function bundleConvertEntityRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();
    $link_templates = [];

    // Find a link template to use as the base route for the local task.
    foreach (['canonical', 'edit-form', 'duplicate-form', 'version-history'] as $rel) {
      if ($entity_type->hasLinkTemplate($rel)) {
        $link_templates[] = $rel;
      }
    }

    // No route to build if there is no link template to use as a base.
    if (empty($link_templates)) {
      return NULL;
    }

    $base = reset($link_templates);
    $route = new Route($entity_type->getLinkTemplate($base) . '/bundle-convert');
    $route
      ->addDefaults([
        '_controller' => '\Drupal\bundle_convert\Controller\BundleConvertController::build',
        '_title' => 'Convert entity bundle',
        'entity_type_id' => $entity_type_id,
      ])
      ->setOption('_admin_route', TRUE)
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ])
      ->setRequirement('_bundle_convert_local_task_access', 'TRUE');

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }

}

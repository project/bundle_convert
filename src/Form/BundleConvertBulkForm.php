<?php

namespace Drupal\bundle_convert\Form;

use Drupal\bundle_convert\BatchProcessor;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides the bundle convert bulk form.
 */
class BundleConvertBulkForm extends BundleConvertConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bundle_convert_bulk_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    if ($form_state->get('step') === 'confirm') {
      return parent::buildForm($form, $form_state);
    }

    $settings = $this->bundleConverter->getBundleConvertSettings();
    // Filter out entity types that don't have bulk enabled in settings.
    $entity_types = array_filter($this->bundleConverter->getApplicableEntityTypes(), function (string $entity_type) use ($settings) {
      return !empty($settings[$entity_type]['bulk']);
    }, ARRAY_FILTER_USE_KEY);

    $entity_type_options = array_map(function ($entity_type) {
      return $entity_type->getLabel();
    }, $entity_types);

    if (empty($entity_type_options)) {
      $form['warning'] = [
        '#markup' => $this->t('You must enable at least one entity type in the @link.', [
          '@link' => Link::createFromRoute('settings form', 'bundle_convert.settings')->toString(),
        ]),
      ];

      return $form;
    }

    $wrapper_id = 'bundle_convert_bulk_form_ajax';
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';

    $form['options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Options'),
    ];

    $form['options']['delete_from_bundle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete bundles'),
      '#description' => $this->t('Automatically delete the bundles being converted from after conversion is complete.'),
    ];

    $selected_entity_type_id = $form_state->getValue('entity_type_id');
    if (!$selected_entity_type_id) {
      $selected_entity_type_id = array_key_first($entity_type_options);
    }
    $bundles = $this->bundleConverter->getApplicableBundles($selected_entity_type_id);

    $form['entity_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Select entity type'),
      '#description' => $this->t('Select the entity type for the bundle that you want to convert to a different bundle.'),
      '#options' => $entity_type_options,
      '#default_value' => $selected_entity_type_id,
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];

    $from_bundles_options = array_map(function ($bundle) {
      return ['bundles' => $bundle['label']];
    }, $bundles);
    $form['from_bundles'] = [
      '#type' => 'tableselect',
      '#header' => [
        'bundles' => $this->t('Convert from bundles'),
      ],
      '#options' => $from_bundles_options,
    ];

    $to_bundle_options = array_map(function ($bundle) {
      return $bundle['label'];
    }, $bundles);
    $form['to_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Convert to bundle'),
      '#description' => $this->t('Select the bundle you wish to convert to.'),
      '#options' => $to_bundle_options,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#submit' => ['::submitConfirm'],
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $step = $form_state->get('step');
    if ($step !== 'confirm') {
      // Filter out from_bundles values that are not selected.
      $from_bundles = array_filter($form_state->getValue('from_bundles'));
      if (empty($from_bundles)) {
        $form_state->setError($form, $this->t('You must choose at least one bundle to convert.'));
      }
      $form_state->setValue('from_bundles', $from_bundles);
    }
  }

  /**
   * Submit handler for the confirm action.
   */
  public function submitConfirm(array &$form, FormStateInterface $form_state) {
    $form_state
      ->set('step', 'confirm')
      ->set('entity_type_id', $form_state->getValue('entity_type_id'))
      ->set('from_bundles', $form_state->getValue('from_bundles'))
      ->set('to_bundle', $form_state->getValue('to_bundle'))
      ->set('options', $form_state->getValue('options'))
      ->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_type_id = $form_state->get('entity_type_id');
    $from_bundles = $form_state->get('from_bundles');
    $to_bundle = $form_state->get('to_bundle');
    $options = $form_state->get('options');

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Converting bundles'))
      ->setProgressMessage('')
      ->setFinishCallback([BatchProcessor::class, 'convertAllFinish'])
      ->addOperation(
        [BatchProcessor::class, 'convertAll'],
        [$entity_type_id, $from_bundles, $to_bundle, $options]
      );

    batch_set($batch_builder->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you wish to convert the bundles?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('bundle_convert.bulk_form');
  }

  /**
   * Ajax refresh callback.
   */
  public function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

}

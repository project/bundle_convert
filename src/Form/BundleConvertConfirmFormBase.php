<?php

namespace Drupal\bundle_convert\Form;

use Drupal\bundle_convert\BundleConverterInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the bundle convert confirm base form.
 */
abstract class BundleConvertConfirmFormBase extends ConfirmFormBase {

  /**
   * The bundle converter service.
   *
   * @var \Drupal\bundle_convert\BundleConverterInterface
   */
  protected $bundleConverter;

  /**
   * The BundleConvertBulkForm constructor.
   *
   * @param \Drupal\bundle_convert\BundleConverterInterface $bundle_converter
   *   The bundle converter service.
   */
  public function __construct(BundleConverterInterface $bundle_converter) {
    $this->bundleConverter = $bundle_converter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bundle_convert.converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $this->messenger()
      ->addWarning($this->t('Do not convert bundles on a live site! Make sure you have a backup of your database!'));

    $form['description'] = $this->buildSummaryElement($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you wish to convert the bundles?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // Using buildSummaryElement instead...need access to form_state
    // to build the selected options.
    return '';
  }

  /**
   * Builds the summary element.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The summary element render array.
   */
  protected function buildSummaryElement(array $form, FormStateInterface $form_state) {
    $element = [];

    $entity_type_id = $form_state->get('entity_type_id');
    $from_bundles = $form_state->get('from_bundles');
    $to_bundle = $form_state->get('to_bundle');

    $entity_types = $this->bundleConverter->getApplicableEntityTypes();
    $entity_type = $entity_types[$entity_type_id];

    $message = $this->formatPlural(count($from_bundles),
      'Are you sure you want to convert the <em>@entity_type</em> bundle to <em>@to_bundle</em> bundle?',
      'Are you sure you want to convert the following <em>@entity_type</em> bundles to <em>@to_bundle</em> bundle?',
      [
        '@entity_type' => $entity_type->getLabel(),
        '@to_bundle' => $to_bundle,
      ]);

    $element['to'] = [
      '#markup' => '<h2>' . $message . '</h2>',
    ];

    $element['from'] = array_map(function ($from_bundle) use ($entity_type_id, $to_bundle) {
      $dropped_fields = array_keys($this->bundleConverter->getOrphanedDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle));
      $empty_fields = array_keys($this->bundleConverter->getEmptyDedicatedFieldTables($entity_type_id, $from_bundle, $to_bundle));

      return [
        '#type' => 'details',
        '#title' => $from_bundle,
        '#open' => TRUE,
        'identical' => [
          '#type' => 'container',
          '#access' => empty($dropped_fields) && empty($empty_fields),
          'description' => [
            '#markup' => '<p>' . $this->t('The <em>@to_bundle</em> bundle has the same fields as the <em>@from_bundle</em> bundle.', [
              '@to_bundle' => $to_bundle,
              '@from_bundle' => $from_bundle,
            ]) . '</p>',
          ],
        ],
        'dropped_fields' => [
          '#type' => 'container',
          '#access' => !empty($dropped_fields),
          'description' => [
            '#markup' => '<h3>' . $this->t('Dropped Fields') . '</h3>',
            '#suffix' => '<p>' . $this->t("The following field's data will be deleted because they do not exist on the <em>@to_bundle</em> bundle.", [
              '@to_bundle' => $to_bundle,
            ]) . '</p>',
          ],
          'list' => [
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#items' => $dropped_fields,
          ],
        ],
        'empty_fields' => [
          '#type' => 'container',
          '#access' => !empty($empty_fields),
          'description' => [
            '#markup' => '<h3>' . $this->t('Empty Fields') . '</h3>',
            '#suffix' => '<p>' . $this->t("The following field's data will be empty because they exist on the <em>@to_bundle</em> bundle but do not exist on the <em>@from_bundle</em> bundle.", [
              '@to_bundle' => $to_bundle,
              '@from_bundle' => $from_bundle,
            ]) . '</p>',
          ],
          'list' => [
            '#theme' => 'item_list',
            '#list_type' => 'ul',
            '#items' => $empty_fields,
          ],
        ],
      ];
    }, $from_bundles);

    return $element;
  }

}

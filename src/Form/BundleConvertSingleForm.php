<?php

namespace Drupal\bundle_convert\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\ConfirmFormHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Provides the bundle convert single form.
 */
class BundleConvertSingleForm extends BundleConvertConfirmFormBase implements BaseFormIdInterface {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'entity_bundle_convert_single_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Get entity type ID from the route because ::buildForm has not yet been
    // called.
    $entity_type_id = $this->getRouteMatch()->getParameter('entity_type_id');
    return $entity_type_id . '_bundle_convert_single_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $entity = NULL) {
    $this->entity = $entity;
    $form['#tree'] = TRUE;

    if ($form_state->get('step') === 'confirm') {
      return parent::buildForm($form, $form_state);
    }

    $bundles = $this->bundleConverter->getApplicableBundles($this->entity->getEntityTypeId());
    // Don't allow selecting the same bundle as the entity.
    if (isset($bundles[$this->entity->bundle()])) {
      unset($bundles[$this->entity->bundle()]);
    }

    $to_bundle_options = array_map(function ($bundle) {
      return $bundle['label'];
    }, $bundles);

    $form['to_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Convert to bundle'),
      '#description' => $this->t('Select the bundle you wish to convert to.'),
      '#options' => $to_bundle_options,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#submit' => ['::submitConfirm'],
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = ConfirmFormHelper::buildCancelLink($this, $this->getRequest());

    return $form;
  }

  /**
   * Submit handler for the confirm action.
   */
  public function submitConfirm(array &$form, FormStateInterface $form_state) {
    $form_state
      ->set('step', 'confirm')
      ->set('entity_type_id', $this->entity->getEntityTypeId())
      ->set('from_bundles', [$this->entity->bundle()])
      ->set('to_bundle', $form_state->getValue('to_bundle'))
      ->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $to_bundle = $form_state->get('to_bundle');
    $success = $this->bundleConverter->convertEntity($this->entity, $to_bundle);

    $this->messenger()->addMessage($this->t('The entity was @success converted to bundle @to_bundle.', [
      '@success' => $success ? 'successfully' : 'unsuccessfully',
      '@to_bundle' => $to_bundle,
    ]), $success ? MessengerInterface::TYPE_STATUS : MessengerInterface::TYPE_ERROR);

    $entity_type = $this->entity->getEntityType();
    if ($entity_type->hasLinkTemplate('edit-form')) {
      $url = $this->entity->toUrl('edit-form');
    }
    elseif ($entity_type->hasLinkTemplate('canonical')) {
      $url = $this->entity->toUrl('canonical');
    }
    else {
      $rel = array_key_first($entity_type->getLinkTemplates());
      $url = $this->entity->toUrl($rel);
    }

    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure wish to convert the bundle?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // This URL will only be utilized if the destination
    // query parameter is not set.
    return Url::fromRoute('<current>');
  }

}

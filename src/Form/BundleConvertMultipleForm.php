<?php

namespace Drupal\bundle_convert\Form;

use Drupal\bundle_convert\BatchProcessor;
use Drupal\bundle_convert\BundleConverterInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\ConfirmFormHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the bundle convert multiple form.
 */
class BundleConvertMultipleForm extends BundleConvertConfirmFormBase implements BaseFormIdInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected $tempStore;

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The selected entities.
   *
   * @var array
   */
  protected $selection = [];

  /**
   * The BundleConvertBulkForm constructor.
   *
   * @param \Drupal\bundle_convert\BundleConverterInterface $bundle_converter
   *   The bundle converter service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(BundleConverterInterface $bundle_converter, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager, PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($bundle_converter);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->tempStore = $temp_store_factory->get('bundle_convert');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bundle_convert.converter'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'entity_bundle_convert_multiple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Get entity type ID from the route because ::buildForm has not yet been
    // called.
    $entity_type_id = $this->getRouteMatch()->getParameter('entity_type_id');
    return $entity_type_id . '_bundle_convert_multiple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $this->entityTypeId = $entity_type_id;
    $this->selection = $this->tempStore->get($this->currentUser->id() . ':' . $entity_type_id);
    $form['#tree'] = TRUE;

    // Show a message if this page is accessed directly.
    if (empty($this->selection)) {
      $form['warning'] = [
        '#markup' => $this->t('You must select at least one entity to convert.'),
      ];
      return $form;
    }

    if ($form_state->get('step') === 'confirm') {
      return parent::buildForm($form, $form_state);
    }

    $bundles = $this->bundleConverter->getApplicableBundles($entity_type_id);
    $to_bundle_options = array_map(function ($bundle) {
      return $bundle['label'];
    }, $bundles);

    $form['to_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Convert to bundle'),
      '#description' => $this->t('Select the bundle you wish to convert to.'),
      '#options' => $to_bundle_options,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#submit' => ['::submitConfirm'],
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = ConfirmFormHelper::buildCancelLink($this, $this->getRequest());

    return $form;
  }

  /**
   * Submit handler for the confirm action.
   */
  public function submitConfirm(array &$form, FormStateInterface $form_state) {
    $form_state
      ->set('step', 'confirm')
      ->set('entity_type_id', $this->entityTypeId)
      ->set('from_bundles', array_keys($this->selection))
      ->set('to_bundle', $form_state->getValue('to_bundle'))
      ->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $to_bundle = $form_state->get('to_bundle');

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Converting bundles'))
      ->setProgressMessage('')
      ->setFinishCallback([BatchProcessor::class, 'convertAllFinish'])
      ->addOperation(
        [BatchProcessor::class, 'convertMultiple'],
        [$this->entityTypeId, $to_bundle, $this->selection]
      );

    batch_set($batch_builder->toArray());

    // Clear the selection.
    $this->tempStore->delete($this->currentUser->id() . ':' . $this->entityTypeId);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->selection),
      'Are you sure you wish to convert the bundle?',
      'Are you sure you wish to convert the bundles?'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // This URL will only be utilized if the destination
    // query parameter is not set.
    return Url::fromRoute('<current>');
  }

}

<?php

namespace Drupal\bundle_convert\Form;

use Drupal\bundle_convert\BundleConverterInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Entity\Action;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the bundle convert settings form.
 */
class BundleConvertSettingsForm extends ConfigFormBase {

  /**
   * The bundle converter service.
   *
   * @var \Drupal\bundle_convert\BundleConverterInterface
   */
  protected $bundleConverter;

  /**
   * The entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected $entityTypes;

  /**
   * The BundleConvertSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\bundle_convert\BundleConverterInterface $bundle_converter
   *   The bundle converter service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BundleConverterInterface $bundle_converter) {
    parent::__construct($config_factory);
    $this->bundleConverter = $bundle_converter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('bundle_convert.converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bundle_convert.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bundle_convert_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#tree'] = TRUE;

    $config = $this->config('bundle_convert.settings');
    $settings = $config->get('entity_types');
    $this->entityTypes = $this->bundleConverter->getApplicableEntityTypes();

    $form['settings'] = [
      '#type' => 'container',
    ];

    foreach ($this->entityTypes as $entity_type) {
      $entity_type_id = $entity_type->id();
      $enable_action = !empty($settings[$entity_type_id]['action']);
      $enable_bulk = !empty($settings[$entity_type_id]['bulk']);
      $enable_local_task = !empty($settings[$entity_type_id]['local_task']);

      // Local task can only be created if there is a base route
      // to attach it to.
      $link_templates = [];
      foreach (['canonical', 'edit-form', 'duplicate-form', 'version-history'] as $rel) {
        if ($entity_type->hasLinkTemplate($rel)) {
          $link_templates[] = str_replace('-', '_', $rel);
        }
      }

      $local_task_available = count($link_templates) > 1;
      $local_task_description = $this->t('Enable the convert bundle local task for this entity type.');
      if (!$local_task_available) {
        $local_task_description = '<em>' . $this->t('Notice: This setting is not available for this entity type because there is no route to attach it to.') . '</em>';
      }

      $form['settings'][$entity_type_id] = [
        '#type' => 'details',
        '#title' => $entity_type->getLabel(),
        '#open' => $enable_action || $enable_bulk || $enable_local_task,
        'action' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable action'),
          '#description' => $this->t('Enable the convert bundle action for this entity type.'),
          '#default_value' => $enable_action,
        ],
        'bulk' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable bulk'),
          '#description' => $this->t('Enable bulk convert bundle for this entity type.'),
          '#default_value' => $enable_bulk,
        ],
        'local_task' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable local task'),
          '#description' => $local_task_description,
          '#default_value' => $enable_local_task,
          '#disabled' => !$local_task_available,
        ],
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('bundle_convert.settings');
    $new_settings = $form_state->getValue('settings');
    $config->set('entity_types', $new_settings);
    $config->save();

    foreach ($new_settings as $entity_type_id => $settings) {
      $this->handleActions($entity_type_id, $settings);
    }

    // Clear the local_task cache so local_tasks can appear enabled.
    \Drupal::service('plugin.manager.menu.local_task')->clearCachedDefinitions();
  }

  /**
   * Handles creating and deleting actions based on settings.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $settings
   *   The settings.
   */
  protected function handleActions($entity_type_id, array $settings = []) {
    $entity_type = $this->entityTypes[$entity_type_id];

    $action_id = $entity_type_id . '_convert_bundle_action';
    $action = Action::load($action_id);
    // If the action is enabled, and it doesn't exist yet.
    if (!empty($settings['action']) && !$action) {
      $action = Action::create([
        'id' => $action_id,
        'label' => $this->t('Convert @entity_type bundle', ['@entity_type' => strtolower($entity_type->getLabel())]),
        'type' => $entity_type_id,
        'configuration' => [],
        'plugin' => 'entity:convert_bundle_action:' . $entity_type_id,
      ]);
      $action->save();
    }
    elseif (empty($settings['action']) && $action) {
      $action->delete();
    }
  }

}

<?php

namespace Drupal\bundle_convert\Plugin\Derivative;

use Drupal\bundle_convert\BundleConverterInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides bundle convert local task.
 */
class BundleConvertLocalTaskDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The bundle converter service.
   *
   * @var \Drupal\bundle_convert\BundleConverterInterface
   */
  protected $bundleConverter;

  /**
   * Constructs a new BundleConvertLocalTaskDeriver object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\bundle_convert\BundleConverterInterface $bundle_converter
   *   The bundle converter service.
   */
  public function __construct(TranslationInterface $string_translation, BundleConverterInterface $bundle_converter) {
    $this->stringTranslation = $string_translation;
    $this->bundleConverter = $bundle_converter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('string_translation'),
      $container->get('bundle_convert.converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (empty($this->derivatives)) {
      $settings = $this->bundleConverter->getBundleConvertSettings();

      foreach ($this->bundleConverter->getApplicableEntityTypes() as $entity_type_id => $entity_type) {
        // Check if the local task is enabled in settings for the entity type.
        if (empty($settings[$entity_type_id]['local_task'])) {
          continue;
        }

        // Find a link template to use as the base route for the local task.
        $link_templates = [];
        foreach (['canonical', 'edit-form', 'duplicate-form', 'version-history'] as $rel) {
          if ($entity_type->hasLinkTemplate($rel)) {
            $link_templates[] = str_replace('-', '_', $rel);
          }
        }

        if (count($link_templates) > 1) {
          $base = reset($link_templates);
          $this->derivatives["entity.$entity_type_id.convert_bundle"] = [
            'route_name' => "entity.$entity_type_id.bundle_convert",
            'title' => $this->t('Convert bundle'),
            'base_route' => "entity.$entity_type_id.$base",
            'weight' => 100,
          ] + $base_plugin_definition;
        }
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
